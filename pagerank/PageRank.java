/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   First version:  Johan Boye, 2012
 */  

import java.util.*;
import java.io.*;

public class PageRank{

    /**  
     *   Maximal number of documents. We're assuming here that we
     *   don't have more docs than we can keep in main memory.
     */
    final static int MAX_NUMBER_OF_DOCS = 2000000;

    /**
     *   Mapping from document names to document numbers.
     */
    Hashtable<String,Integer> docNumber = new Hashtable<String,Integer>();

    /**
     *   Mapping from document numbers to document names
     */
    String[] docName = new String[MAX_NUMBER_OF_DOCS];

    /**  
     *   A memory-efficient representation of the transition matrix.
     *   The outlinks are represented as a Hashtable, whose keys are 
     *   the numbers of the documents linked from.<p>
     *
     *   The value corresponding to key i is a Hashtable whose keys are 
     *   all the numbers of documents j that i links to.<p>
     *
     *   If there are no outlinks from i, then the value corresponding 
     *   key i is null.
     */
    Hashtable<Integer,Hashtable<Integer,Boolean>> link = new Hashtable<Integer,Hashtable<Integer,Boolean>>();

    /**
     *   The number of outlinks from each node.
     */
    int[] out = new int[MAX_NUMBER_OF_DOCS];

    /**
     *   The number of documents with no outlinks.
     */
    int numberOfSinks = 0;

    /**
     *   The probability that the surfer will be bored, stop
     *   following links, and take a random jump somewhere.
     */
    final static double BORED = 0.15;

    /**
     *   Convergence criterion: Transition probabilities do not 
     *   change more that EPSILON from one iteration to another.
     */
    final static double EPSILON = 0.0001;

    /**
     *   Never do more than this number of iterations regardless
     *   of whether the transistion probabilities converge or not.
     */
    final static int MAX_NUMBER_OF_ITERATIONS = 1000;

    double[][] G;
    double[] x;

    
    /* --------------------------------------------- */


    public PageRank( String filename ) {
	int noOfDocs = readDocs( filename );
    G = new double[noOfDocs][noOfDocs];
    for(int i=0; i<G.length; i++) {
        if(link.containsKey(i) && out[i]!=0) {
            for(int j=0; j<noOfDocs; j++) {
                if(link.get(i).containsKey(j) && link.get(i).get(j)) {
                    G[i][j] = (BORED / noOfDocs) + ((1.0 - BORED) / out[i]);
                }
                else {
                    G[i][j] = (BORED / noOfDocs);
                }
            }
        }
        else {
            for(int j=0; j<noOfDocs; j++) {
                G[i][j] = 1.0 / noOfDocs;
            }
        }
    }
	computePagerank( noOfDocs );

    // int[] indexes = new int[noOfDocs];
    // for(int i=0; i<noOfDocs; i++) {
    //     indexes[i] = i+1;
    // }

    final double[] ranked = new double[noOfDocs];
        Integer[] indices = new Integer[noOfDocs];
        for (int i = 0; i < noOfDocs; i++) {
            ranked[i] = x[i];
            indices[i] = i;
        }
        Arrays.sort(indices, new Comparator<Integer>() {
            @Override
            public int compare(final Integer o1, final Integer o2) {
                return Double.compare(ranked[o1], ranked[o2]);
            }
        });
        for (int i = 0; i < 50; i++) {
            int idx = indices[indices.length - i - 1];
            System.out.println((i + 1) + ": " + docName[idx] + " "
                               + ranked[idx]);
        }

    // for(int i=1; i<x.length;i++) {
    //     for(int j=i; j>0; j--) {
    //         if(x[j-1]<x[j]) {
    //             //swap
    //             int swapInt = indexes[j-1];
    //             indexes[j-1] = indexes[j];
    //             indexes[j] = swapInt;
    //             double swapDouble = x[j-1];
    //             x[j-1] = x[j];
    //             x[j] = swapDouble;
    //         }
    //     }
    // }

    // try {
    //     File file = new File("results.txt");
    //     FileWriter fw = new FileWriter(file.getAbsoluteFile());
    //     for(int i=0; i<x.length; i++) {
    //         fw.write(docName[indexes[i]] + ":" + x[i] + "\n");
    //     }
    //     fw.close();
    // }
    // catch(IOException i) {
    //     i.printStackTrace();
    // }
    }


    /* --------------------------------------------- */


    /**
     *   Reads the documents and creates the docs table. When this method 
     *   finishes executing then the @code{out} vector of outlinks is 
     *   initialised for each doc, and the @code{p} matrix is filled with
     *   zeroes (that indicate direct links) and NO_LINK (if there is no
     *   direct link. <p>
     *
     *   @return the number of documents read.
     */
    int readDocs( String filename ) {
	int fileIndex = 0;
	try {
	    System.err.print( "Reading file... " );
	    BufferedReader in = new BufferedReader( new FileReader( filename ));
	    String line;
	    while ((line = in.readLine()) != null && fileIndex<MAX_NUMBER_OF_DOCS ) {
		int index = line.indexOf( ";" );
		String title = line.substring( 0, index );
		Integer fromdoc = docNumber.get( title );
		//  Have we seen this document before?
		if ( fromdoc == null ) {	
		    // This is a previously unseen doc, so add it to the table.
		    fromdoc = fileIndex++;
		    docNumber.put( title, fromdoc );
		    docName[fromdoc] = title;
		}
		// Check all outlinks.
		StringTokenizer tok = new StringTokenizer( line.substring(index+1), "," );
		while ( tok.hasMoreTokens() && fileIndex<MAX_NUMBER_OF_DOCS ) {
		    String otherTitle = tok.nextToken();
		    Integer otherDoc = docNumber.get( otherTitle );
		    if ( otherDoc == null ) {
			// This is a previousy unseen doc, so add it to the table.
			otherDoc = fileIndex++;
			docNumber.put( otherTitle, otherDoc );
			docName[otherDoc] = otherTitle;
		    }
		    // Set the probability to 0 for now, to indicate that there is
		    // a link from fromdoc to otherDoc.
		    if ( link.get(fromdoc) == null ) {
			link.put(fromdoc, new Hashtable<Integer,Boolean>());
		    }
		    if ( link.get(fromdoc).get(otherDoc) == null ) {
			link.get(fromdoc).put( otherDoc, true );
			out[fromdoc]++;
		    }
		}
	    }
	    if ( fileIndex >= MAX_NUMBER_OF_DOCS ) {
		System.err.print( "stopped reading since documents table is full. " );
	    }
	    else {
		System.err.print( "done. " );
	    }
	    // Compute the number of sinks.
	    for ( int i=0; i<fileIndex; i++ ) {
		if ( out[i] == 0 )
		    numberOfSinks++;
	    }
	}
	catch ( FileNotFoundException e ) {
	    System.err.println( "File " + filename + " not found!" );
	}
	catch ( IOException e ) {
	    System.err.println( "Error reading file " + filename );
	}
	System.err.println( "Read " + fileIndex + " number of documents" );
	return fileIndex;
    }


    /* --------------------------------------------- */


    /*
     *   Computes the pagerank of each document.
     */
    void computePagerank( int numberOfDocs ) {
        int iterNumber = 0;
        x = new double[numberOfDocs];
        for(int i=0; i<x.length; i++) {
            // x[i] = 1.0 / x.length;
            x[i] = 0.0;
        }
        x[0] = 1.0;
        while(iterNumber<MAX_NUMBER_OF_ITERATIONS) {
            double[] tmp = new double[numberOfDocs];
            for(int i=0; i<numberOfDocs; i++) {
                tmp[i] = 0;
                for (int j=0; j<numberOfDocs; j++)
                    tmp[i] += G[j][i] * x[j];   // dot product of i-th row in A with the column vector b
            }
            // calculate the length of the resultant vector
            double norm_sq=0;
            for (int k=0; k<numberOfDocs; k++)
                norm_sq += tmp[k]*tmp[k]; 
            norm_sq = Math.sqrt(norm_sq);
            boolean converged = false;
            double distanceManhattan = 0.0;
            for(int i=0; i<numberOfDocs; i++) {
                distanceManhattan = Math.max(Math.abs(x[i]-tmp[i]), distanceManhattan);
            }
            System.out.println("Distance: " + distanceManhattan);
            if(distanceManhattan<EPSILON) {
                converged = true;
            }
            // normalize b to unit vector for next iteration
            for (int k=0; k<numberOfDocs; k++) {
                x[k] = tmp[k]/norm_sq;
                //x[k] = tmp[k];
            }
            if(converged)
                break;
            iterNumber++;
            System.out.println("iteration number : " + iterNumber);
        }
    }


    /* --------------------------------------------- */


    public static void main( String[] args ) {
	if ( args.length != 1 ) {
	    System.err.println( "Please give the name of the link file" );
	}
	else {
	    new PageRank( args[0] );
	}
    }
}
