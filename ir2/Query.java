/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   First version:  Hedvig Kjellström, 2012
 */  

package ir2;

import java.util.*;

public class Query {
    
    public LinkedList<String> terms = new LinkedList<String>();
    public LinkedList<Double> weights = new LinkedList<Double>();
    public boolean weightsSet;

    /**
     *  Creates a new empty Query 
     */
    public Query() {
        weightsSet = false;
    }
	
    /**
     *  Creates a new Query from a string of words
     */
    public Query( String queryString  ) {
	StringTokenizer tok = new StringTokenizer( queryString );
    weightsSet = false;
	while ( tok.hasMoreTokens() ) {
	    terms.add( tok.nextToken() );
	    weights.add( new Double(1) );
	}    
    }

    public boolean isWeightsSet() {
        return weightsSet;
    }
    
    /**
     *  Returns the number of terms
     */
    public int size() {
	return terms.size();
    }
    
    /**
     *  Returns a shallow copy of the Query
     */
    public Query copy() {
	Query queryCopy = new Query();
	queryCopy.terms = (LinkedList<String>) terms.clone();
	queryCopy.weights = (LinkedList<Double>) weights.clone();
	return queryCopy;
    }
    
    /**
     *  Expands the Query using Relevance Feedback
     */
    public void relevanceFeedback( PostingsList results, boolean[] docIsRelevant, Indexer indexer ) {
	// results contain the ranked list from the current search
	// docIsRelevant contains the users feedback on which of the 10 first hits are relevant
        double a = 1.0, b = 0.8;

        HashMap<String, Double> documentVectorSum = new HashMap<String, Double>();
        int numberOfrelevantDocs = 0;
        for(int i=0; i<docIsRelevant.length; i++) {
            if(docIsRelevant[i]) {
                numberOfrelevantDocs++;
                int docID = results.get(i).docID;
                Iterator<String> words = indexer.index.getDictionary();
                while(words.hasNext()) {
                    String word = words.next();
                    PostingsList pListofWord = indexer.index.getPostings(word);
                    if(pListofWord!=null) {
                        PostingsEntry pEntryOfDocInWord = pListofWord.fetchPostingsEntry(docID);
                        if(pEntryOfDocInWord!=null) {
                            double docLength = indexer.index.docLengths.get("" + docID);
                            double tfIdf = pEntryOfDocInWord.termFrequency() * pListofWord.inverseDocumentFrequency() / docLength;
                            if(documentVectorSum.containsKey(word)) {
                                double temp = documentVectorSum.get(word);
                                documentVectorSum.put(word, temp + tfIdf);
                            }
                            else {
                                documentVectorSum.put(word, tfIdf);
                            }
                        }
                    }
                }
            }
        }
        for(int i=0; i<terms.size(); i++) {
            weights.set(i, a * weights.get(i));
            if(documentVectorSum.containsKey(terms.get(i))) {
                weights.set(i, weights.get(i) + b * documentVectorSum.get(terms.get(i)) / numberOfrelevantDocs);
            }
        }

        for(String i: documentVectorSum.keySet()) {
            if(terms.contains(i)) {
                continue;
            }
            else {
                terms.add(i);
                weights.add(documentVectorSum.get(i));
            }
        }
        
        weightsSet = true;

        // if(size()>3) {
        //     for(int i=0; i<size(); i++) {
        //         for(int j=i+1; j<size(); j++) {
        //             if(weights.get(i)<weights.get(j)) {
        //                 String tempToken = terms.get(i);
        //                 double tempWeight = weights.get(i);
        //                 terms.set(i, terms.get(j));
        //                 weights.set(i, weights.get(j));
        //                 terms.set(j, tempToken);
        //                 weights.set(j, tempWeight);
        //             }
        //         }
        //     }
        // }

        // while(size()>3) {
        //     terms.remove(2);
        //     weights.remove(2);
        // }
    }
}

    
