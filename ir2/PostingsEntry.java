/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   First version:  Johan Boye, 2010
 *   Second version: Johan Boye, 2012
 *   Third version: Vishnu Ramesh Maroli, 2016 :P
 */  

package ir2;

import java.io.Serializable;
import java.util.*;

public class PostingsEntry implements Comparable<PostingsEntry>, Serializable {
    
    public int docID;
    public double score;
    //public double similarity;
    public LinkedList<Integer> offset = new LinkedList<Integer>();

    /**
     *  PostingsEntries are compared by their score (only relevant 
     *  in ranked retrieval).
     *
     *  The comparison is defined so that entries will be put in 
     *  descending order.
     */

    // public void setSimilarity(double _similarity) {
    //     similarity = _similarity;
    // }

    public int compareTo( PostingsEntry other ) {
	   return Double.compare( other.score, score );
    }

    public static Comparator<PostingsEntry> sortScoreDescending 
                          = new Comparator<PostingsEntry>() {

        public int compare(PostingsEntry fruit1, PostingsEntry fruit2) {
            // compareTo does descending

            //descending order
            return fruit1.compareTo(fruit2);

            //ascending order
            //return fruit2.compareTo(fruit1);
        }

    };

    // public int compareToSimilarity( PostingsEntry other ) {
    //    return Double.compare( other.similarity, similarity );
    // }

    // public static Comparator<PostingsEntry> sortSimilarityDescending 
    //                       = new Comparator<PostingsEntry>() {

    //     public int compare(PostingsEntry fruit1, PostingsEntry fruit2) {
    //         // compareTo does descending

    //         //descending order
    //         return fruit1.compareToSimilarity(fruit2);

    //         //ascending order
    //         //return fruit2.compareToSimilarity(fruit1);
    //     }

    // };

    public double termFrequency() {
        return offset.size();
    }

    public boolean equals(PostingsEntry other) {
        //System.out.println("equals called for " + docID + " and " + other.docID);
        if(other.docID==docID) {
            return true;
        }
        else {
            return false;
        }
    }

    //
    //  YOUR CODE HERE
    //
    PostingsEntry() {
    }

    PostingsEntry(int _docID, double _score, int _offset) {
        docID = _docID;
        score = _score;
        offset.add(_offset);
    }

}