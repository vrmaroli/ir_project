/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   First version:  Johan Boye, 2010
 *   Second version: Johan Boye, 2012
 *   Additions: Hedvig Kjellstrom, 2012-14
 */  


package ir2;

import java.util.*;


/**
 *   Implements an inverted index as a Hashtable from words to PostingsLists.
 */
public class HashedIndex implements Index {

    /** The index as a hashtable. */
    public HashMap<String,PostingsList> index = new HashMap<String,PostingsList>();


    /**
     *  Inserts this token in the index.
     */
    public void insert( String token, int docID, int offset ) {
        /*
            check if the word exists in index
            if it exists
                if docID exists
                    increment score
                else
                    create a postings entry with docID and Offset
                    Add to the list
            else
                create a postings list
                create a postings entry with docID and Offset
                Add to the list
        */
        if(index.containsKey(token)) {
            PostingsList postingsListForToken = index.get(token);
            if(postingsListForToken.list.getLast().docID==docID) {
                postingsListForToken.list.getLast().score++;
                postingsListForToken.list.getLast().offset.add(offset);
            }
            else {
                PostingsEntry newPostingsEntry = new PostingsEntry(docID, 1, offset);
                postingsListForToken.list.add(newPostingsEntry);
            }
        }
        else {
            PostingsList newPostingsList = new PostingsList();
            PostingsEntry newPostingsEntry = new PostingsEntry(docID, 1, offset);
            newPostingsList.list.add(newPostingsEntry);
            index.put(token, newPostingsList);
        }
    }


    /**
     *  Returns all the words in the index.
     */
    public Iterator<String> getDictionary() {
    // 
    //  REPLACE THE STATEMENT BELOW WITH YOUR CODE
    //
        return index.keySet().iterator();
    }


    /**
     *  Returns the postings for a specific term, or null
     *  if the term is not in the index.
     */
    public PostingsList getPostings( String token ) {
    // 
    //  REPLACE THE STATEMENT BELOW WITH YOUR CODE
    //
        if(index.containsKey(token)) {
            return index.get(token);
        }
        else {
            return null;
        }
    }

    public double tfIdf(int _docID, String token) {
        PostingsList pListOfToken = getPostings(token);
        if(pListOfToken==null) {
            return 0;
        }
        PostingsEntry pEntryOf_docID = pListOfToken.fetchPostingsEntry(_docID);
        if(pEntryOf_docID==null) {
            return 0;
        }
        pEntryOf_docID.score = (1.0 + Math.log10(pEntryOf_docID.termFrequency())) * pListOfToken.inverseDocumentFrequency();
        return pEntryOf_docID.score;
    }

    public double tfIdf_docLength(int _docID, String token) {
        PostingsList pListOfToken = getPostings(token);
        if(pListOfToken==null) {
            return 0;
        }
        PostingsEntry pEntryOf_docID = pListOfToken.fetchPostingsEntry(_docID);
        if(pEntryOf_docID==null) {
            return 0;
        }
        double docLength = docLengths.get(""+_docID);
        pEntryOf_docID.score = pEntryOf_docID.termFrequency() * pListOfToken.inverseDocumentFrequency() / docLength;
        return pEntryOf_docID.score;
    }


    /**
     *  Searches the index for postings matching the query.
     */
    public PostingsList search( Query query, int queryType, int rankingType, int structureType ) {
    // 
    //  REPLACE THE STATEMENT BELOW WITH YOUR CODE
    //
        for(String i: query.terms) {
            System.out.print(i + " ");
        }
        System.out.println("");
        if(queryType==INTERSECTION_QUERY) {
            System.out.println("INTERSECTION_QUERY");
            if(query.size()==1) {
                return getPostings(query.terms.getFirst());
            } else if (query.size()>1) {
                PostingsList firstPostingsList = getPostings(query.terms.getFirst());
                if(firstPostingsList==null) {
                    return null;
                }
                PostingsList ret = new PostingsList(firstPostingsList);
                for(int i=1; i<query.size(); i++) {
                    ret = ret.intersection(getPostings(query.terms.get(i)));
                }
                return ret;
            } else {
                return null;
            }
        }
        else if(queryType==PHRASE_QUERY) {
            System.out.println("PHRASE_QUERY");
            if(query.size()==1) {
                return getPostings(query.terms.getFirst());
            }
            else if (query.size()>1) {
                PostingsList ret = new PostingsList();
                PostingsList firstPostingsList = getPostings(query.terms.getFirst());
                if(firstPostingsList==null) {
                    return null;
                }
                for(int i=0;i<firstPostingsList.size();i++) {
                    boolean addToReturn = false;
                    PostingsEntry postingsEntryAtI = firstPostingsList.list.get(i);
                    for(int x = 0; x<postingsEntryAtI.offset.size(); x++) {
                        int offset = postingsEntryAtI.offset.get(x);
                        for(int j=1; j<query.size(); j++) {
                            PostingsList jthPostingsList = getPostings(query.terms.get(j));
                            PostingsEntry postingEntryWithDocID = jthPostingsList.fetchPostingsEntry(postingsEntryAtI.docID);
                            if(postingEntryWithDocID==null) {
                                break;
                            }
                            else if(postingEntryWithDocID.offset.contains(offset + j)) {
                                if(j==query.size()-1) {
                                    addToReturn = true;
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }
                    if(addToReturn) {
                        ret.list.add(postingsEntryAtI);
                    }
                }
                return ret;

            }
            else {
                return null;
            }
        }
        else if(queryType==RANKED_QUERY) {
            System.out.println("RANKED_QUERY");
            if(query.size()==1) {
                PostingsList ret = search(query, INTERSECTION_QUERY, TF_IDF, UNIGRAM);
                for(PostingsEntry i:ret.list) {
                    tfIdf_docLength(i.docID, query.terms.getFirst());
                }
                // Sort on descending order of score
                Collections.sort(ret.list, PostingsEntry.sortScoreDescending);
                // Setting query weight
                // ignoring relevance feedback for one word queries
                return ret;
            }
            else {
                //  multi word queries
                //  find tfIdf of query
                //  hashmap<docID, similarity> similarityOfDocumentWithQuery
                //  PostingsList listOfPostingsEntries
                //  forAll terms in query
                //      forAll docs in getPostings(term)
                //          if hashmap has an entry for postingsEntry.docID
                //              continue
                //          else
                //              add postingsEntry to listOfPostingsEntries
                //              calculate similarity with query and populate similarityOfDocumentWithQuery (hashmap)
                //  sort listOfPostingsEntries according to similarity
                //  return listOfPostingsEntries
                HashMap<String, Double> tfIdfQuery = new HashMap<String, Double>();
                if(query.isWeightsSet()) {
                    for(int i=0; i<query.size(); i++) {
                        tfIdfQuery.put(query.terms.get(i), query.weights.get(i));
                    }
                }
                else {
                    for(String i: query.terms) {
                        if(tfIdfQuery.containsKey(i))
                            tfIdfQuery.put(i, tfIdfQuery.get(i) + 1.0);
                        else
                            tfIdfQuery.put(i, 1.0);
                    }
                    for(String i: tfIdfQuery.keySet()) {
                        PostingsList pList = getPostings(i);
                        if(pList==null)
                            tfIdfQuery.put(i, 0.0);
                        else {
                            tfIdfQuery.put(i, (1.0 + Math.log(tfIdfQuery.get(i))) * pList.inverseDocumentFrequency() );
                            //tfIdfQuery.put(i, (1.0 + Math.log(tfIdfQuery.get(i))) * pList.inverseDocumentFrequency() / query.size());
                        }
                    }
                    for(int i=0; i<query.size(); i++) {
                        query.weights.set(i, tfIdfQuery.get(query.terms.get(i)));
                    }
                    query.weightsSet = true;
                }
                HashMap<String, Double> similarityOfDocumentWithQuery = new HashMap<String, Double>();
                PostingsList ret = new PostingsList();
                for(String i: tfIdfQuery.keySet()) {
                    if(tfIdfQuery.get(i)==0.0)
                        continue;
                    PostingsList termPList = getPostings(i);
                    System.out.println("\tToken:\t" + i + "\tdocs:\t" + termPList.size());
                    for(PostingsEntry j: termPList.list) {
                        if(similarityOfDocumentWithQuery.containsKey("" + j.docID)) {
                            double tfidf_Document = (1.0 + Math.log10(j.termFrequency())) * termPList.inverseDocumentFrequency();
                            double numerator = tfIdfQuery.get(i) * tfidf_Document;
                            numerator += similarityOfDocumentWithQuery.get("" + j.docID);
                            similarityOfDocumentWithQuery.put("" + j.docID, numerator);
                        }
                        else {
                            double tfidf_Document = (1.0 + Math.log10(j.termFrequency())) * termPList.inverseDocumentFrequency();
                            double numerator = tfIdfQuery.get(i) * tfidf_Document;
                            similarityOfDocumentWithQuery.put("" + j.docID, numerator);
                        }
                    }
                }
                System.out.println("similarityOfDocumentWithQuery calculated!!!");
                for(String i: similarityOfDocumentWithQuery.keySet()) {
                    double docLength = docLengths.get(i);
                    double similarity = similarityOfDocumentWithQuery.get(i) / (query.terms.size() * docLength);
                    //similarityOfDocumentWithQuery.put(i, similarity);
                    PostingsEntry j = new PostingsEntry(Integer.parseInt(i), similarity, 1);
                    ret.list.add(j);
                }

                // sort ret
                Collections.sort(ret.list, PostingsEntry.sortScoreDescending);
                return ret;
            }
        }
        else {
            return null;
        }
    }


    /**
     *  No need for cleanup in a HashedIndex.
     */
    public void cleanup() {
    }
}