/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   First version:  Johan Boye, 2010
 *   Second version: Johan Boye, 2012
 *   Additions: Hedvig Kjellstrom, 2012-14
 */  


package ir;

import java.util.*;
import java.io.*;


/**
 *   Implements an inverted index as a Hashtable from words to PostingsLists.
 */
public class HashedIndex implements Index {

    /** The index as a hashtable. */
    public HashMap<String,PostingsList> index = new HashMap<String,PostingsList>();

    public boolean tokenExistsInIndex(String token) {
        // return indexedTokens.contains(token);
        File tokenDir = new File("index/" + token);
        if(tokenDir.exists() && !tokenDir.isDirectory()) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     *  Inserts this token in the index.
     */
    public void insert( String token, int docID, int offset ) {
        /*
            check if the word exists in index
            if it exists
                if docID exists
                    increment score
                else
                    create a postings entry with docID and Offset
                    Add to the list
            else
                create a postings list
                create a postings entry with docID and Offset
                Add to the list
            load token to a PostingsList object
            perform necessary updates
            write PostingsList back to disk
        */
        try {
            File file = new File("index/" + token);
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            //BufferedWriter bw = new BufferedWriter(fw);
            fw.write(docID + "\n" + offset + "\n");
            //bw.write(_score + "\n");
            //fw.write();
            fw.close();
        } catch(IOException i) {
            i.printStackTrace();
        }
    }


    /**
     *  Returns all the words in the index.
     */
    public Iterator<String> getDictionary() {
    // 
    //  REPLACE THE STATEMENT BELOW WITH YOUR CODE
    //
    return null;
    }


    /**
     *  Returns the postings for a specific term, or null
     *  if the term is not in the index.
     */
    public PostingsList getPostings( String token ) {
    // 
    //  REPLACE THE STATEMENT BELOW WITH YOUR CODE
    //
        if(tokenExistsInIndex(token)) {
            PostingsList postingsListForToken = new PostingsList();
            postingsListForToken.readFromFile(token);
            return postingsListForToken;
        }
        else {
            return null;
        }
    }


    /**
     *  Searches the index for postings matching the query.
     */
    public PostingsList search( Query query, int queryType, int rankingType, int structureType ) {
    // 
    //  REPLACE THE STATEMENT BELOW WITH YOUR CODE
    //
        if(queryType==INTERSECTION_QUERY) {
            System.out.println("INTERSECTION_QUERY");
            if(query.size()==1) {
                return getPostings(query.terms.getFirst());
            } else if (query.size()>1) {
                PostingsList firstPostingsList = getPostings(query.terms.getFirst());
                if(firstPostingsList==null) {
                    return null;
                }
                PostingsList ret = new PostingsList(firstPostingsList);
                for(int i=1; i<query.size(); i++) {
                    ret = ret.intersection(getPostings(query.terms.get(i)));
                }
                return ret;
            } else {
                return null;
            }
        }
        else if(queryType==PHRASE_QUERY) {
            System.out.println("PHRASE_QUERY");
            if(query.size()==1) {
                return getPostings(query.terms.getFirst());
            }
            else if (query.size()>1) {
                PostingsList ret = new PostingsList();
                PostingsList firstPostingsList = getPostings(query.terms.getFirst());
                if(firstPostingsList==null) {
                    return null;
                }
                for(int i=0;i<firstPostingsList.size();i++) {
                    boolean addToReturn = false;
                    PostingsEntry postingsEntryAtI = firstPostingsList.list.get(i);
                    for(int x = 0; x<postingsEntryAtI.offset.size(); x++) {
                        int offset = postingsEntryAtI.offset.get(x);
                        for(int j=1; j<query.size(); j++) {
                            PostingsList jthPostingsList = getPostings(query.terms.get(j));
                            PostingsEntry postingEntryWithDocID = jthPostingsList.fetchPostingsEntry(postingsEntryAtI.docID);
                            if(postingEntryWithDocID==null) {
                                break;
                            }
                            else if(postingEntryWithDocID.offset.contains(offset + j)) {
                                if(j==query.size()-1) {
                                    addToReturn = true;
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }
                    if(addToReturn) {
                        ret.list.add(postingsEntryAtI);
                    }
                }
                return ret;

            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }


    /**
     *  No need for cleanup in a HashedIndex.
     */
    public void cleanup() {
    }
}
