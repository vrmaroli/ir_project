/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   First version:  Johan Boye, 2010
 *   Second version: Johan Boye, 2012
 */  

package ir;

import java.io.Serializable;
import java.util.LinkedList;

public class PostingsEntry implements Comparable<PostingsEntry>, Serializable {
    
    public int docID;
    public double score;
    public LinkedList<Integer> offset = new LinkedList<Integer>();

    /**
     *  PostingsEntries are compared by their score (only relevant 
     *  in ranked retrieval).
     *
     *  The comparison is defined so that entries will be put in 
     *  descending order.
     */
    public int compareTo( PostingsEntry other ) {
	   return Double.compare( other.score, score );
    }

    public boolean equals(PostingsEntry other) {
        //System.out.println("equals called for " + docID + " and " + other.docID);
        if(other.docID==docID) {
            return true;
        }
        else {
            return false;
        }
    }

    //
    //  YOUR CODE HERE
    //
    PostingsEntry() {
    }

    PostingsEntry(int _docID, double _score, int _offset) {
        docID = _docID;
        score = _score;
        offset.add(_offset);
    }

}