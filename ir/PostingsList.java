/*  
 *   This file is part of the computer assignment for the
 *   Information Retrieval course at KTH.
 * 
 *   First version:  Johan Boye, 2010
 *   Second version: Johan Boye, 2012
 */  

package ir;

import java.util.LinkedList;
import java.util.Arrays;
import java.io.Serializable;
import java.io.*;

/**
 *   A list of postings for a given word.
 */
public class PostingsList implements Serializable {
    
    /** The postings list as a linked list. */
    public LinkedList<PostingsEntry> list = new LinkedList<PostingsEntry>();

    public void writeToFile(String token) {
        try {
            File file = new File("index/" + token);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            for(int i=0; i<list.size(); i++) {
                for (int j : list.get(i).offset) {
                    bw.write(list.get(i).docID + "\n");
                    bw.write(list.get(i).score + "\n");
                    bw.write(j + "\n");
                }
            }
            bw.close();

        } catch(IOException i) {
            i.printStackTrace();
        }
    }

    public void readFromFile(String token) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("index/" + token));
            String line1 = br.readLine();
            while(line1!=null) {
                String line2 = br.readLine();
                // String line3 = br.readLine();
                int _docID = Integer.parseInt(line1);
                int _offset = Integer.parseInt(line2);
                // System.out.println(_docID + " " + _offset);
                //double _score = Double.parseDouble(line2);
                PostingsEntry temp = fetchPostingsEntry(_docID);
                if(temp==null) {
                    temp = new PostingsEntry(_docID, 1, _offset);
                    list.add(temp);
                }
                else {
                    temp.score = 1;
                    temp.offset.add(_offset);
                }
                line1 = br.readLine();
            }
        }
        catch(IOException i) {
            i.printStackTrace();
        }
    }

    /**  Number of postings in this list  */
    public int size() {
	   return list.size();
    }

    /**  Returns the ith posting */
    public PostingsEntry get( int i ) {
	   return list.get( i );
    }

    public PostingsEntry fetchPostingsEntry(int _docID) {
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i).docID==_docID) {
                return list.get(i);
            }
        }
        return null;
    }

    PostingsList() {
    }

    PostingsList(PostingsList other) {
        for(int i=0; i<other.size(); i++) {
            list.add(other.get(i));
        }
    }

    public PostingsList intersection(PostingsList other) {
        PostingsList ret = new PostingsList();
        for(int i=0; i<size(); i++) {
            PostingsEntry temp = other.fetchPostingsEntry(get(i).docID);
            if(temp==null) {
                // not present
            }
            else {
                // present
                ret.list.add(get(i));
            }
        }
        return ret;
    }

    //
    //  YOUR CODE HERE
    //
}